#NN/LM Drupal Transitional Theme

This repository contains all code for the "transitional" theme intended to succeed the nnlm.gov website theme developed in conjunction between WebSTOC and AIR.  This theme is an adaptation of the Drupal [Omega](https://drupal.org/project/omega) 3 theme.  It has been developed to include subtheme enhancements for each of the individual regions.  It is a fixed-width adaptive theme based on the 960 grid system, and uses 4 different breakpoints to respond to different screen widths.

##Project structure
The general structure of the sass folder bears discussion:

		├── sass
		│   ├── archive
		│   │   ├── default
		│   │   └── global
		│   ├── default
		│   ├── global
		│   ├── gmr
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── mar
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── mcr
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── narrow
		│   ├── national
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── ner
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── normal
		│   ├── pnr
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── print
		│   │   └── global
		│   ├── psr
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── scr
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   ├── sea
		│   │   ├── default
		│   │   ├── global
		│   │   ├── narrow
		│   │   ├── normal
		│   │   └── wide
		│   └── wide

The four breakpoints in the theme are as follows:

- **global**: includes definitions that are active for all displays, as well as definitions for mobile display.
- **narrow**: all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)
- **normal**: all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)
- **wide**: all and (min-width: 1220px)


These breakpoints have folders in src/sass that define defaults for the theme.  The primary file in each of these folders is _style.scss.  That file includes other sass files during compass compilation.  Each NN/LM region also has a folder, which contains the four breakpoints as folders within.  It is in these folders that regional overrides are placed.

During compilation, the variables in src/sass/_base.scss are read first, and then immediately overridden by src/sass/[region]/_[region].scss if applicable.  See src/sass/gmr/global/style.scss for an example of how this works.

Note that Omega 3 uses javascript to do media detection, and assigns classes to the page body to dictate a particular breakpoint style.  For instance, if the mobile style is found to be active, the class 'responsive-layout-mobile' is added to the page body.

##Usage notes

This package is maintained in git using a simplified version [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow/) workflow methodology.  It currently has 2 distinct branches, 'develop' and 'master'.  All modifications should be done either on the develop branch, or a branch cloned from develop.

This package uses Grunt for build tooling, and should *not* be directly copied into place.  To deploy this module to the NN/LM development instance of Drupal, first [set up your system](http://gruntjs.com/getting-started) to handle Grunt build tooling, and then invoke 'grunt sandy-dev'.  To deploy to staging and production, commit all changes to the 'master' branch, and push to the remote repository.  The CI server will detect the change and test and deploy the code from there.
)