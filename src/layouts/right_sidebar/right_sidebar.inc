<?php

// Plugin definition
$plugin = array(
  'title' => t('Right Sidebar'),
  'category' => t('NN/LM theme specific layouts'),
  'icon' => 'right_sidebar.png',
  'theme' => 'right_sidebar',
  'css' => 'right_sidebar.css',
  'regions' => array(
    'center' => t('Center content'),
    'right' => t('Right Sidebar')
  ),
);
