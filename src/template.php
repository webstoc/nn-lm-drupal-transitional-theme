<?php

use Drupal\nnlm_core\Utilities as U;

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/**
 * Override or insert variables into the html template.
 */
function nnlm_preprocess_html(&$vars) {

  // Add conditional CSS for IE7 and below.
  //global script additions
  if (!module_exists('libraries')) {
    watchdog('Libraries module missing', 'The required module "Libraries" was not found');
    return;
  }
  /* External styles */
  //maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css
  $external_styles = array(
    'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'
  );
  foreach($external_styles as $url){
    drupal_add_css($url, array(
      'type'=>'file',
      'group' => CSS_THEME,
      'external'=>TRUE,
      'every_page' => TRUE,
      'media'=>'all',
      'preprocess'=>TRUE));
  }
  /* Theme scripts */


  $theme_scripts = array(
    'Google font loader' => '//ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js'
  );
  if (!empty($theme_scripts)) {
    foreach ($theme_scripts as $name => $url) {
      drupal_add_js($url, array('type' => 'external', 'scope' => 'footer', 'group' => JS_THEME, 'every_page' => TRUE));
    }
  }

  // Setup IE meta tag to force IE rendering mode
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' => 'IE=edge,chrome=1',
      'http-equiv' => 'X-UA-Compatible',
    ),
    '#weight' => '-99999',
  );

  // Add header meta tag for IE to head
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
}

function nnlm_preprocess_region(&$vars) {
  //nnlm_core_dump($vars, "Preprocessing region. Vars ");
  if (in_array($vars['elements']['#region'], array('content'))) {
    $theme = alpha_get_theme();
    dpm("Preprocessing region ", $vars['elements']['#region']);
    switch ($vars['elements']['#region']) {
      case 'content':
        $vars['breadcrumb'] = $theme->page['breadcrumb'];
        //nnlm_core_dump($vars, "Content");
        break;
      default:
      break;
    }
  }
}

function nnlm_preprocess_node(&$vars){
  //nnlm_core_dump($vars, "Preprocessing node. Vars ");
  //BOOKMARK: need to find a way to add scald image links around the outside
  //of the images.
  return;

  if($vars['type'] !== 'vsx_slide'){
    return;
  }
  dpm("Processing slide ".$vars['title']);
  $image_link = ($vars['field_slide_image_link'][0]['url']) ?: NULL;
  if(is_null($image_link)){
    dpm("Image link is empty");
    return;
  }
  dpm("Image link: ".$image_link);
  $image =& $vars['field_slide_image'][0];
  $image['#theme'] = 'link';
  $image['#path'] = $image_link;
  $image['#options']['attributes'] = array(
    'data-image-link'=>$image_link
  );
   /*array(
      '#theme' => 'link',
      '#text' => drupal_render($img),
      '#path' => "node/" . $node->nid,
      '#options' => array(
        'attributes' => array(),
        'html' => TRUE,
      ),
    );*/
  static $first = FALSE;
  if(!$first){
    $first = TRUE;
    dpm("Vars");
    dpm($vars);
  }
}
function nnlm_media_formatter_large_icon($variables) {
  //dpm(__FUNCTION__);
}

