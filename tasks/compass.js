module.exports = {
	outputstyle: 'compressed',
	src: { // Target
		options: { // Target options
			sassDir: 'src/sass',
			cssDir: 'src/css'
		}
	}
};