module.exports = {
	main: {
		options: {
			tasks: [
				'build', 
				'clean', 
				'default', 
				'help',
				'jshint',
				'phplint',
				'sandy-dev'], 
			filter: 'include',
			descriptions: {
				'default': 'Alias for help',
				'help': "Displays available tasks",
				'build': 'Builds a deployable version of the rosters app to the build/ directory in the root of this project',
				'phplint': 'Performs php syntax checking on all php files in this project',
				'jshint': 'Performs javascript syntax checking on all js files in this project',
				'sandy-dev': 'Deploys this code to the development instance of Drupal'
			},
			groups: {
				'Code validation Grunt tasks': ['jshint', 'phplint'],
				'Cleanup Grunt tasks': ['clean'],
				'Primary Grunt tasks': ['default', 'build', 'help'],
				'Other Grunt tasks': ['availabletasks']
			}
		}
	}
};