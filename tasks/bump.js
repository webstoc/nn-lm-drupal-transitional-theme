//see https://github.com/vojtajina/grunt-bump
module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-prompt');
	var output = {
		options: {
			files: ['package.json'], //try to maintain all variable changes in package.json
			updateConfigs: [], //probably unnecessary - this task is run independently of others.
			commit:  grunt.config('bump.prompt.commit') || false, //true, once debugging is complete?
			commitMessage: grunt.config('bump.prompt.commitMessage') || 'Release v%VERSION%',
			commitFiles: ['package.json'],
			createTag:  grunt.config('bump.prompt.createTag') || false, //true, once debugging is complete?
			tagName: grunt.config('bump.prompt.tagName') || 'v%VERSION%',
			tagMessage: grunt.config('bump.prompt.tagMessage') || 'Version %VERSION%',
			push: false, //true, once debugging is complete?
			pushTo: 'origin',
			gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
			globalReplace: false,
			prereleaseName: 'dev',
			regExp: false
		}
	};
	return output;
};