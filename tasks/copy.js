module.exports = {
	build: {
		files: [
			{
				cwd: 'deps/free-file-icons',
				expand: true,
				src: [
					'**'
				],
				dest: 'build/free-file-icons'
			},
			{
				cwd: 'src/js/edgefonts',
				expand: true,
				src: [
					'**'
				],
				dest: 'build/js/edgefonts'
			},
			{
				cwd: 'src/js',
				expand: true,
				src: [
					'ckeditor.config.js'
				],
				dest: 'build'
			},
			{
				cwd: 'src',
				expand: true,
				src: [
					'img/favicon.ico',
					'img/nlm_logo.svg',
					'img/nlm-logo.gif',
					'img/nlm_nih_logo.svg',
					'img/nlm_nih_logo.png',
					'img/nnlm_logo.svg',
					'img/nnlm_logo_100p.gif',
					'img/nnlm_map.svg',
					'img/nnlm_map_optimized.svg',
					'img/pubmed_icon.gif',
					'img/search_icon*',
					'layouts/**',
					'logo.png',
					'preprocess/**',
					'process/**',
					'screenshot.png',
					'template.php',
					'templates/**'
				],
				dest: 'build'
			}
		]
	},
	localhost: {
		files: [
			// includes files within path and its sub-directories
			{
				cwd: 'build',
				expand: true,
				mode: true,
				src: [
					'**'
				],
				dest: '/Users/aronbeal/Sites/drupal/sites/all/themes/nnlm'
			},
			{
				cwd: 'src/js',
				expand: true,
				mode: true,
				src: [
					'script-dev.js'
				],
				dest: '/Users/aronbeal/Sites/drupal/sites/all/themes/nnlm/js/'
			}
		]
	}
};