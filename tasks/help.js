module.exports = function(grunt) {
  grunt.registerTask('help', 'Shows how to use', function() {
  	var README = grunt.file.read('tasks/README.md');
    grunt.log.writeln(README);
    grunt.task.run('availabletasks');
  });
};