module.exports = function(grunt) {
	return {
		options: {
			context: {
				project_description: '<%= project_description %>',
				javascript_filename: '<%= javascript_filename %>',
				stylesheet_filename: '<%= stylesheet_filename %>',
				version_timestamp: '<%= version_timestamp %>',
				version_number: '<%= version_number %>'
			}
		},
		build: {
			files: {
				'build/nnlm.info': 'src/nnlm.info'
			}
		}
	};
};