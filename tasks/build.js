module.exports = function(grunt) {
	grunt.registerTask("build", [
		//'lint', //syntax check rosters files
		'clean:default', //removes tmp and build
		'compass:src', //builds css from sass and scss files
		'concat:js', //creates single JS file
		'concat:css',
	  'preprocess:build',
	  'copy:build',
	  'uglify:build'
	]);
};