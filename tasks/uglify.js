module.exports = {
	options: {
		mangle: false,
		sourceMap: false
	},
	build: {
		files: [{
			expand: true,
			cwd: 'build/js',
			src: '<%=javascript_filename %>',
			dest: 'build/js'
		}]
	}
};