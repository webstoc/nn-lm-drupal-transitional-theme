/** 
 * Runs tasks concurrently if possible
 */
module.exports = {
	lint: ['jshint', 'phplint']
};