module.exports = function(grunt) {
  //see docs at https://github.com/gruntjs/grunt-contrib-watch
  var options = {
    debounceDelay: 500, //ms
    livereload: true,
    reload: true,
    spawn: false
  };
  var deploytarget = 'dev'; //one of 'dev' or 'stage', as defined in rsync.js
  return {
    scripts: {
      files: ['src/js/**/*.js'],
      tasks: ['jshint:default', 'concat:js', 'preprocess:build', 'copy:build', 'uglify:build', 'rsync:'+deploytarget],
      options: options
    },
    sass: {
      files: ['src/sass/**/*.scss'],
      tasks: ['scsslint', 'compass:src', 'concat:css', 'preprocess:build', 'copy:build', 'uglify:build', 'rsync:'+deploytarget],
      options: options
    },
    build: {
      files: ['src/nnlm.info', 'src/templates/**', 'src/template.php'],
      tasks: ['phplint:default', 'copy:build', 'uglify:build', 'rsync:'+deploytarget],
      options: options
    }
  };
};